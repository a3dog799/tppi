
import java.util.Scanner;


public class jogo_da_forca {
	private static String[] args;

	public static void main(String[] args) {
		boolean acertou = false;
		int qtd = 0;
		int contaErros = 0;
		String senha, tenta;
		Scanner scan = new Scanner(System.in);
		String boneco = "\0";
		
		System.out.println("Jogo da Forca.");
		System.out.print("Digite a palavra a ser descoberta:");
		
		String v = scan.next();
	
		char formaPalavra[] = new char[v.length()];
		String apresentaPalavra = "\0";
		for (int y = 0; y < v.length(); y++) {
			formaPalavra[y] = '_';
			apresentaPalavra += formaPalavra[y] + " ";
		}
		
		for (int i = 0; i < 50; i++) {
			System.out.println("");
		}
		
		System.out.println("Inicio do jogo");

			System.out.println("");
		
		for (int j = 0; j < v.length() + 6; j++) // tentativas = qtd de palavras + 6 erros
		{
			acertou = false;
			
			System.out.println(boneco + "Tentativa: " + (j + 1) + " de " + (v.length() + 6) + "\n"
					+ apresentaPalavra + "\n Digite a letra:");
			
			 senha = scan.next();
			 
			 for (int i = 0; i < 50; i++) {
					System.out.println("");
				}
			
			apresentaPalavra = "\0";
			for (int i = 0; i < v.length(); i++) {
				tenta = v.substring(i, i + 1);
				if (senha.trim().equals(tenta)) {
					acertou = true;
					qtd++;
					formaPalavra[i] = senha.charAt(0); // converte de String para
					apresentaPalavra = apresentaPalavra + formaPalavra[i] + " ";
					if (qtd == v.length()) {
						
						for (int c = 0; c < 50; c++) {
							System.out.println("");
						}
						
						System.out.print(boneco + "Voc� Ganhou! PalavraCompleta: " + v + "\nTotal de Tentativas: " + (j + 1));
						menu();
					}
				} else {
					apresentaPalavra += formaPalavra[i] + " ";
				}
			}
			if (acertou == false) {
				contaErros++;
				boneco = "\0";
				switch (contaErros) {
				case 1:
					boneco = " ___O \n";
					boneco += "|\t \n";
					boneco += "|\t \n";
					boneco += "|\t \n";
					break;
				case 2:
					boneco = " ___O \n";
					boneco += "|   | \n";
					boneco += "|\n";
					boneco += "|\n";
					break;
				case 3:
					boneco = " ___O \n";
					boneco += "|  -| \n";
					boneco += "|\n";
					boneco += "|\n";
					break;	
				case 4:
					boneco = " ___O \n";
					boneco += "|  -|-\n";
					boneco += "|\n";
					boneco += "|\n";
					break;
				case 5:
					boneco = " ___O \n";
					boneco += "|  -|-\n";
					boneco += "|  | \n";
					boneco += "|\n";
					break;
				case 6:
					boneco = " ___O \n";
					boneco += "|  -|-\n";
					boneco += "|  | | \n";
					boneco += "|        \n";
					break;
				}
				System.out.println("\nVoc� ERROU " + contaErros + " vez(es)");
				if (contaErros >= 6) {
					System.out.print(boneco + "PERDEU! FIM DO JOGO");
					menu();
				}
			}
		}
	}
	
	public static void menu() {
		Scanner scan = new Scanner(System.in);
		int tecla;
		System.out.println("");
		System.out.println("[(-1-)] Voltar a Jogar este jogo");
        System.out.println("[(-8-)] Sair do Jogo/Programa");
        System.out.println("[(-9-)] Voltar ao Menu");
		System.out.println("");
		System.out.print("Opcao: ");
		tecla = scan.nextInt();

		  
          
          switch(tecla)
 	     {
 	   
          
 	      case 1:
 	    	jogo_da_forca.main(args);
	 		break;
 		
 	     case 8:
 	    	 System.out.println ("Obrigado e Adeus.");
 	 		break;
 	 		
 	     case 9:
 	    	 Menu.main(args);
 	    	
 			    break;
 	  default:
	 	    System.err.println ( "Digito desconhecido" );
	 	 
	 	    break;
 	 		
 	    
 	 	
 	     }
	}
	
}