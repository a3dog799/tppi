import java.util.Random;
import java.util.Scanner;


public class Tabuleiro {
    private int[][] minas;
    private char[][] tabuleiro;
    private int linha, coluna;
    Random random = new Random();
    Scanner entrada = new Scanner(System.in);
    
    public Tabuleiro(){
        minas = new int[7][7];
        tabuleiro = new char[7][7];
        iniciaMinas(); // coloca 0 em todas as posi��es do tabuleiro de minas
        sorteiaMinas(); //coloca, aleatoriamente, 7 minas no tabuleiro de minas
        preencheDicas();//preenche o tabuleiro de minas com o n�mero de minas vizinhas
        iniciaTabuleiro();//inicia o tabuleiro de exibi��o com _
        
    }
    
    public boolean ganhou(){
        int count=0;
        for(int line = 1 ; line < 6 ; line++)
            for(int column = 1 ; column < 6 ; column++)
                if(tabuleiro[line][column]=='_')
                    count++;
        if(count == 7)
            return true;
        else
            return false;                
    }
    
    public void abrirVizinhas(){
    	  
    	  
        for(int i=-1 ; i<2 ; i++)
            for(int j=-1 ; j<2 ; j++)
                if( (minas[linha+i][coluna+j] != -1) && (linha != 0 && linha != 6 && coluna != 0 && coluna != 6) ){
                    tabuleiro[linha+i][coluna+j]=Character.forDigit(minas[linha+i][coluna+j], 7);
                }
    }
    
    public int getPosicao(int linha, int coluna){
        return minas[linha][coluna];
        
        
    }
    
    public boolean setPosicao(){
            
            do{
                System.out.print("\nLinha: "); 
                linha = entrada.nextInt();
                System.out.print("Coluna: "); 
                coluna = entrada.nextInt();
            
                
                if( linha >= 1 && linha <= 5 && coluna >= 1 && coluna <= 5) {
                if( (tabuleiro[linha][coluna] != '_') && ((linha < 6 && linha > 0) && (coluna < 6 && coluna > 0))) {
                    System.out.println("Esse campo j� est� sendo exibido");
                }
            }else 
            	           	
                
                    System.out.println("Escolha n�meros de 1 at� 5");
            
                
            }while((linha < 1 || linha > 5) || (coluna < 1 || coluna > 5) || (tabuleiro[linha][coluna] != '_') );
            
            if(getPosicao(linha, coluna)== -1)
                return true;
            else
                return false;
            
    }
    
    public void exibe(){
        System.out.println("\n     Linhas");
        for(int linha = 5 ; linha > 0 ; linha--){
            System.out.print("       "+linha + " ");
            
            for(int coluna = 1 ; coluna < 6 ; coluna++){
                    System.out.print("   "+ tabuleiro[linha][coluna]);
            }
                
            System.out.println();
        }
            
        System.out.println("\n            1   2   3   4   5 ");
        System.out.println("                      Colunas");
        
    }
    
    public void preencheDicas(){
        for(int line=1 ; line < 6 ; line++)
            for(int column=1 ; column < 6 ; column++){
                
                    for(int i=-1 ; i<=1 ; i++)
                        for(int j=-1 ; j<=1 ; j++)
                            if(minas[line][column] != -1)
                                if(minas[line+i][column+j] == -1)
                                    minas[line][column]++;
                
            }
            
    }
    
    public void exibeMinas(){
        for(int i=1 ; i < 6; i++)
            for(int j=1 ; j < 6 ; j++)
                if(minas[i][j] == -1)
                    tabuleiro[i][j]='*';
        
        exibe();
    }
    
    public void iniciaTabuleiro(){
        for(int i=1 ; i<minas.length ; i++)
            for(int j=1 ; j<minas.length ; j++)
                tabuleiro[i][j]= '_';
    }
    
    public void iniciaMinas(){
        for(int i=0 ; i<minas.length ; i++)
            for(int j=0 ; j<minas.length ; j++)
                minas[i][j]=0;
    }
    
    public void sorteiaMinas(){
        boolean sorteado;
        int linha, coluna;
        for(int i=0 ; i<7 ; i++){
            
            do{
                linha = random.nextInt(5) + 1;
                coluna = random.nextInt(5) + 1;
                
                if(minas[linha][coluna] == -1)
                    sorteado=true;
                else
                    sorteado = false;
            }while(sorteado);
            
            minas[linha][coluna] = -1;
        }
    }
}